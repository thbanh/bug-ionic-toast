import {
  IonButton,
  IonContent,
  IonFooter,
  IonHeader,
  IonItem,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonToast,
} from "@ionic/react";
import ExploreContainer from "../components/ExploreContainer";
import "./Home.css";

const Home: React.FC = () => {
  const [presentToast, dismissToast] = useIonToast();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Blank</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer />
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <IonItem>
            <IonLabel>Cocuuo</IonLabel>
          </IonItem>
          <IonButton
            expand="full"
            fill="clear"
            onClick={() => presentToast("test Hello", 2000)}
          >
            Hello at the top
          </IonButton>
          <IonButton
            // type="submit"
            expand="full"
            fill="clear"
            // className="ion-margin-top"
            onClick={() => presentToast("test Ckick", 2000)}
          >
            click
          </IonButton>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  );
};

export default Home;
